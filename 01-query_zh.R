# Leipzig Workshop

# install packages
# install histtext (script)
# install tidyverse (install)

# Load packages
library(histtext)
library(tidyverse)
library(scales)
library(igraph)
library(tidygraph)

# save objects in .RData file
save.image('zwh.RData')
# export file

# Re-upload saved RData file
load(file = "zwh.RData")

# List available corpora
histtext::list_corpora()

## Basic search function

#Search in Chinese publications (Shenbao)
zwh <- search_documents('"資委會"|"資源委員會"|"全國資源委員會"', "shunpao-revised")

# Extract the year from the date and count number of documents per year
zwh %>% 
  mutate(Year = stringr::str_sub(Date,0,4)) %>%
  group_by(Year) %>% 
  count(sort = TRUE) # sort by descending order

# Plot distribution over time 

zwh %>% 
  mutate(Year = stringr::str_sub(Date,0,4)) %>%
  group_by(Year) %>% 
  count() %>% 
  ggplot(aes(Year,n)) + geom_col() + 
  labs(title = "The National Resources Commission in the Shenbao",
       subtitle = "Number of articles mentioning the NRC",
       x = "Year", 
       y = "Number of articles",
       caption = "Based on the GetHong digital Shenbao")

# create a new variable for year for further research

zwh <- zwh %>% mutate(Year = stringr::str_sub(Date,0,4))

# Distribution over time: Alternative method [Problem with lubridate]

histtext::count_documents('"資委會"|"資源委員會"|"全國資源委員會"', "shunpao-revised") %>% 
  mutate(date=lubridate::as_date(Date)) %>% 
  mutate(Year= lubridate::year(date)) %>% filter(Year >=1920) %>%
  group_by(Year) %>% summarise(N=sum(N)) %>%
  ggplot(aes(Year,N)) + geom_col() + 
  labs(title = "The National Resources Commission in the Shenbao",
       subtitle = "Number of articles mentioning the NRC",
       x = "Year", 
       y = "Number of articles",
       caption = "Based on the GetHong digital Shenbao")

histtext::count_documents('"資委會"|"資源委員會"|"全國資源委員會"', "shunpao-revised") %>% 
  mutate(date=lubridate::as_date(Date)) %>% 
  mutate(Year= lubridate::year(date)) %>% filter(Year >=1945) %>%
  group_by(Year) %>% summarise(N=sum(N)) %>%
  ggplot(aes(Year,N)) + geom_col() + 
  labs(title = "The National Resources Commission in the Shenbao",
       subtitle = "Number of articles mentioning the NRC",
       x = "Year", 
       y = "Number of articles",
       caption = "Based on the GetHong digital Shenbao")

# Filter by year: 2 methods

# Filter in the query 

zwh_filtered <- search_documents_ex('"資委會"|"資源委員會"|"全國資源委員會"', "shunpao-revised", dates="[1945 TO 1950]")

# Filter in the results of the general query 

zwh_filtered2 <- zwh %>% filter(Year >=1945)



# Retrieve the full text 

zwh_ft <- histtext::get_documents(zwh, "shunpao") 

## retrieve category of document 

# list metadata available

list_search_fields("shunpao")
list_filter_fields("shunpao")

# retrieve category of document  [Irrelevant here, but relevant for DFZZ]

zwh_ft_cat <- histtext::get_search_fields_content(zwh, "shunpao",
                                                  search_fields = c("title", "text"),
                                                  verbose = FALSE) 


## Search concordance 

zwh_conc <- search_concordance('"資委會"|"資源委員會"|"全國資源委員會"', corpus = "shunpao-revised", context_size = 500)


#Search in Chinese publications (Wikipedia)
zwhwiki <- search_documents('"資委會"|"資源委員會"|"全國資源委員會"', "wikibio-zh")
#Search in Chinese publications (Dongfang zazhi)
zwhdf <- search_documents('"資委會"|"資源委員會"|"全國資源委員會"', "dongfangzz")

# Inspect length of articles
zwh_ft <- zwh_ft %>% mutate(LengTx = str_count(Text))

# Select only the articles with less than 1,000 characters
zwh_ft_shrt <- zwh_ft %>% filter(LengTx < 1001)



# Save results and export as csv file

write.csv(zwh, "zwh.csv")
write.csv(zwh_ft, "zwh_ft.csv")
write.csv(zwh_conc, "zwh_conc.csv")
write.csv(zwh_ft_shrt, "zwh_ft_shrt.csv")


############ 

## Extract named entities 

# NER in Chinese
zwh_ner <- ner_on_corpus(zwh_ft_shrt, corpus = "shunpao-revised")


# Distribution of entities 

zwh_ner %>% 
  group_by(Type) %>% 
  count(sort=TRUE)


# Filter relevant entities (PERSON, ORG)

zwh_ner_filtered <- zwh_ner %>% filter(Type %in% c("PERSON", "ORG"))

# count length of named entities 
zwh_ner_filtered <- zwh_ner_filtered %>% mutate(length = nchar(Text))

# separate persons and org (we will treat them separately)

zwh_ner_pers <- zwh_ner_filtered %>% filter(Type == "PERSON")
zwh_ner_org <- zwh_ner_filtered %>% filter(Type == "ORG")

# remove punctuation from persons 

zwh_ner_pers_clean <- zwh_ner_pers %>% 
  mutate(Text_clean = stringr::str_remove_all(Text, "[[:punct:]]")) %>% 
  relocate(Text_clean, .after = Text)

# remove non-persons

zwh_ner_pers_clean <- zwh_ner_pers_clean %>% 
  mutate(Text_clean = str_replace_all(Text_clean, "孫拯", "孫"))

# remove org with less than 2 characters

zwh_ner_org_filtered <- zwh_ner_org %>% filter(length >= 2)

# atomize lists of entities

zwh_ner_org_filtered <- zwh_ner_org_filtered %>% mutate(org1=str_extract(Text,"[^·]+·")) 

# use org1 to retrieve the remaining entities from original text 
# repeat the two operations in succession until we have exhaust the entire list (the maximum is 6 entities)

zwh_ner_org_filtered <- zwh_ner_org_filtered %>% 
  mutate(org2 = str_remove_all(Text, org1)) %>%  
  mutate(org3=str_extract(org2,"[^·]+·")) %>% 
  mutate(org4 = str_remove_all(org2, org3)) %>%  
  mutate(org5=str_extract(org4,"[^·]+·")) %>% 
  mutate(org6 = str_remove_all(org4, org5)) %>%  
  mutate(org7 = str_extract(org6,"[^·]+·")) %>% 
  mutate(org8 = str_remove_all(org6, org7)) %>%  
  mutate(org9 = str_extract(org8,"[^·]+·")) %>% 
  mutate(org10 = str_remove_all(org8, org9)) %>%  
  mutate(org11 = str_extract(org10,"[^·]+·")) %>% 
  mutate(org12 = str_remove_all(org10, org11))

#  retain only org with non pair numbers (1, 3, 5, 7, 9, 11) + the last org12

zwh_ner_org_filtered <- zwh_ner_org_filtered %>% select(-c(org2, org4, org6, org8, org10))

#  remove residual punctuation 

zwh_ner_org_filtered <- zwh_ner_org_filtered  %>% 
  mutate(Text_clean = str_remove_all(Text,"·"))  %>% 
  mutate(Text_clean = str_remove_all(Text_clean,"。"))%>%
  mutate(org1 = str_remove_all(org1,"·")) %>% 
  mutate(org3 = str_remove_all(org3,"·")) %>% 
  mutate(org5 = str_remove_all(org5,"·")) %>% 
  mutate(org7 = str_remove_all(org7,"·")) %>% 
  mutate(org9 = str_remove_all(org9,"·")) %>% 
  mutate(org11 = str_remove_all(org11,"·")) 

# reshape the dataframe to distribute organizations into a single column

zwh_org_list <- zwh_ner_org_filtered %>%
  pivot_longer(
    cols = starts_with("org"),
    names_to = "ORG2",
    names_prefix = "wk",
    values_to = "Text2",
    values_drop_na = TRUE
  )

zwh_ner_org_clean <- zwh_ner_org_filtered %>% select(-c(org1, org3, org5, org7, org9, org11, org12)) %>% 
  relocate(Text_clean, .after = Text)

zwh_org_list <- zwh_org_list %>% select(DocId, Type, Text, Text2, Start, End, Confidence, length) %>% rename(Text_clean = Text2)

zwh_ner_org_final <- bind_rows(zwh_ner_org_clean, zwh_org_list) 
zwh_ner_org_final <- zwh_ner_org_final %>% unique()


#  extract last characters to identify the type of organization 

zwh_ner_org_final <- zwh_ner_org_final  %>% 
  mutate(Text_clean = str_remove_all(Text_clean, "長$")) %>%
  mutate(suff1 = str_sub(Text_clean, - 1, - 1)) %>% 
  mutate(suff2 = str_sub(Text_clean, - 2, - 1)) %>% 
  relocate(suff1, .after = Text_clean) %>%  
  relocate(suff2, .after = suff1) 

#### Standardize names ####

# Build a two-mode network (person-organization) with igraph/tidygraph

# create node list 

node_org <- zwh_ner_org_final %>% 
  group_by(Text_clean) %>% add_tally() %>% # first add a weight to the node (number of mentions)
  select(Text_clean, Type, n) %>% 
  rename(name = Text_clean, weight = n)  %>% 
  unique()

node_pers <- zwh_ner_pers_clean %>% 
  group_by(Text_clean) %>% add_tally() %>% # first add a weight to the node (number of mentions)
  select(Text_clean, Type, n) %>% 
  rename(name = Text_clean, weight = n)  %>% 
  unique() %>% 
  filter(!name == "雷諾")

node_list <- bind_rows(node_pers, node_org) %>% unique()


intersect(node_org$name, node_pers$name)

# create edge list 

edge_org <- zwh_ner_org_final %>% select(DocId, Text_clean)  %>% rename(ORG = Text_clean) %>% unique()
edge_pers <- zwh_ner_pers_clean %>% select(DocId, Text_clean)  %>% rename(PERSON = Text_clean) %>% unique()
edge_list <- inner_join(edge_pers, edge_org) %>% unique()
tidy_edge_list <- edge_list %>% select(PERSON, ORG) %>%
  rename(from = PERSON, to = ORG)


# create igraph object  
ig <- graph_from_data_frame(d=tidy_edge_list, directed = FALSE, vertices=(node_list))

# index nodes shape/color on nodes type 
V(ig)[node_list$Type == "PERSON"]$shape <- "circle"
V(ig)[node_list$Type == "ORG"]$shape <- "square"
V(ig)[node_list$Type == "PERSON"]$color <- "red"
V(ig)[node_list$Type == "ORG"]$color <- "light blue"

# plot with igraph 
plot.igraph(ig, vertex.size = 3, 
            vertex.label.color = "black", 
            vertex.label.cex = 0.3, 
            main="資委會 Affiliation network")

# convert into a tidy graph object
tg <- tidygraph::as_tbl_graph(ig) %>% 
  activate(nodes) %>% 
  mutate(label=name) 

# project in padagraph
tg %>% histtext::in_padagraph("ZWHNetwork") 

# get the URL 
tg %>% histtext::get_padagraph_url("ZWHNetwork") # DOES NOT WORK?

# https://pdg.enpchina.eu/rstudio?gid=ZWHNetwork

no.clusters(ig)
clusters(ig)$csize


# save results 

write.csv(node_list, "node_list.csv")
write.csv(edge_list, "edge_list.csv")