# Basic text analysis with R

# load packages

library(quanteda)
library(tidyverse)
library(tidytext)
library(widyr)
library(igraph)
library(tidygraph)
library(ggraph)

# load data 

zwh_tok <- read.csv("/data/user/h/chenriot/rworkshop/zwh_tok2.csv")
zwk_tok <- zwh_tok2_filtered

# create corpus object 
zwh_corpus <- corpus(zwh_tok$Text, docnames = zwh_tok$DocId)
summary(zwh_corpus)

# extract first document
cat(texts(zwh_corpus[1]))

# create a Document-Term matrix (DTM)
DTM <- tokens(zwh_corpus, include_docvars = TRUE)
DTM

# create a Document-Frequency matrix (DFM)
DFM <- dfm(DTM)
DFM

# Dimensionality of the DTM (number of docs / number of terms = size of vocabulary) 
dim(DFM)

# word frequencies
wordfreq <- as.data.frame(colSums(DFM)) %>% 
  rownames_to_column("words") %>% 
  mutate(freq= colSums(DFM)) %>% select(words, freq) %>% 
  mutate(length = nchar(words))

word_freq_long <- wordfreq %>% filter(length>1)

# plot word/rank frequencies
plot(wordfreq$freq, type = "l", lwd=2, 
     main = "Rank frequency Plot", 
     xlab="Rank", ylab ="Frequency")

plot(word_freq_long$freq, type = "l", lwd=2, 
     main = "Rank frequency Plot", 
     xlab="Rank", ylab ="Frequency")

# 
plot(wordfreq$freq , type = "l", log="xy", lwd=2, 
     main = "Rank-Frequency Plot", 
     xlab="log-Rank", ylab ="log-Frequency")

plot(word_freq_long$freq , type = "l", log="xy", lwd=2, 
     main = "Rank-Frequency Plot", 
     xlab="log-Rank", ylab ="log-Frequency")

# remove stop words 
library(stopwords)
# https://stopwords.quanteda.io/

# list all sources 
stopwords::stopwords_getsources()
# list languages for a specific source: 3 sources for Chinese
stopwords::stopwords_getlanguages("marimo") # two Chinese: zh_tw (taiwan) and zh_cn
stopwords::stopwords_getlanguages("stopwords-iso")
stopwords::stopwords_getlanguages("misc")


# extract stop words from different dictionaries 

zh_iso_stopwords <- stopwords(language = "zh", source = "stopwords-iso")
zh_marimo_tw <- stopwords(language = "zh_tw", source = "marimo")
zh_misc <- stopwords(language = "zh", source = "misc")

stop_iso <- as.data.frame(zh_iso_stopwords) 
stop_iso <- stop_iso %>% mutate(word = zh_iso_stopwords)
stop_iso$zh_iso_stopwords <- NULL

stop_marimo <- as.data.frame(zh_marimo_tw) 
stop_marimo <- stop_marimo %>% mutate(word = zh_marimo_tw)
stop_marimo$zh_marimo_tw <- NULL

stop_misc <- as.data.frame(zh_misc) 
stop_misc <- stop_misc %>% mutate(word = zh_misc)
stop_misc$zh_misc <- NULL

# Combine all stop words

stop_all <- bind_rows(stop_iso, stop_marimo, stop_misc) %>% unique()

# co-occurrences

library(tidytext)

# unnest token from full text, remove stop words and one-character words

zwh_tok_token <- zwh_tok %>% 
  unnest_tokens(output = word, input = Text) %>% 
  filter(!word %in% stop_all$word) %>% 
  mutate(length = nchar(word)) %>% 
  filter(length>1)
  

# word frequency 
zwh_token_count <- zwh_tok_token %>% 
  group_by(word) %>% 
  tally() %>% 
  arrange(desc(n))

# tf-idf per doc

zwh_token_tf_idf_doc <- zwh_tok_token %>%
  count(DocId, word) %>%
  bind_tf_idf(word, DocId, n) %>%
  arrange(desc(tf_idf))

# tf-idf per year
zwh_token_tf_idf_year <- zwh_tok_token %>% 
  count(year, word) %>% 
  bind_tf_idf(word, year, n) %>%
  arrange(desc(tf_idf))

# 5 top words per year

zwh_tok_token <- zwh_tok_token %>% mutate(year= stringr::str_sub(DocId,5,8)) 

top5_word <- zwh_tok_token %>% 
  group_by(year, word) %>% 
  tally() %>% 
  arrange(year, desc(n)) %>% 
  group_by(year) %>% 
  top_n(5)

# 10 top words per year
top10_word <- zwh_tok_token %>% 
  group_by(year, word) %>% 
  tally() %>% 
  arrange(year, desc(n)) %>% 
  group_by(year) %>% 
  top_n(10)

# plot / compare top 10 words for the first and last year 

top10_word %>% 
  filter(year %in% c("1935", "1949"))%>%
  group_by(year) %>%
  top_n(10, n) %>%
  ungroup() %>%
  mutate(word = reorder(word, n)) %>%
  ggplot(aes(n, word, fill = year)) +
  geom_col(show.legend = FALSE) +
  facet_wrap(~ year, scales = "free", ncol=2) +
  labs(x = "Number of occurrences", y = "Word", 
       title = "Most frequent words in the 資委會 corpus", 
       subtitle = "The ten most frequent words on the first and last year", 
       caption = "Based on Shenbao")

# top words per article

top5_word_doc <- zwh_tok_token %>% 
  group_by(DocId, year, word) %>% 
  tally() %>% 
  arrange(desc(n)) %>% 
  group_by(DocId, year, word) %>% 
  top_n(5)

# co-occurrences

library(widyr)
word_pairs <- zwh_tok_token %>%
  pairwise_count(word, DocId, sort = TRUE)

word_pairs %>%
  filter(n > 25) %>%
  graph_from_data_frame() %>%
  ggraph(layout = "fr") +
  geom_edge_link(aes(edge_alpha = n), show.legend = FALSE) +
  geom_node_point(color = "lightblue", size = 5) +
  geom_node_text(aes(label = name), repel = TRUE, point.padding  = unit(0.2, "lines")) +
  theme_void()+
  labs(title = "Word co-occurrences in the 資委會 corpus", 
       subtitle = "Most frequent pairs (n>25)", 
       caption = "Based on Shenbao")

# word correlation through article

word_cors <- zwh_tok_token %>%
  group_by(word) %>%
  filter(n() >= 10) %>%
  pairwise_cor(word, DocId, sort = TRUE)

set.seed(2021)

word_cors %>%
  filter(correlation > .3) %>%
  graph_from_data_frame() %>%
  ggraph(layout = "fr") +
  geom_edge_link(aes(edge_alpha = correlation), show.legend = FALSE) +
  geom_node_point(color = "orange", size = 5) +
  geom_node_text(aes(label = name), repel = TRUE) +
  theme_void()+
  labs(title = "Word co-occurrences in the 資委會 corpus", 
       subtitle = "Pairs of words that show at least a .3 correlation of appearing within the article", 
       caption = "Based on Shenbao")

# focus on specific words 

word1_cor <- word_cors %>%
  filter(item1 == "日本")
word2_cor <- word_cors %>%
  filter(item2 == "學生")
returned_cor <- bind_rows(word1_cor, word2_cor)

# bigrams

zwh_bigram <- zwh_tok %>% 
  unnest_tokens(output = bigram, 
                input = Text, 
                token = "ngrams", 
                n = 2)

# count bigrams 

zwh_bigram_count <- zwh_bigram %>%
  count(bigram, sort = TRUE)

# bigrams per article 

zwh_bigram_tf_idf_doc <- zwh_bigram %>%
  count(DocId, bigram) %>%
  bind_tf_idf(bigram, DocId, n) %>%
  arrange(desc(tf_idf))


# separate bigrams into words 
zwh_bigram_separated <- zwh_bigram %>%
  separate(bigram, c("word1", "word2"), sep = " ")

# network of bigrams 
zwh_bigrams_count2 <- zwh_bigram_separated  %>% 
  count(word1, word2, sort = TRUE)

# filter for only relatively common combinations (over 10)
zwh_bigram_graph <- zwh_bigrams_count2 %>%
  filter(n > 10) %>%
  graph_from_data_frame()

# visualize as a graph

set.seed(2017)

a <- grid::arrow(type = "closed", length = unit(.05, "inches"))

ggraph(zwh_bigram_graph, layout = "fr") +
  geom_edge_link(aes(edge_alpha = n), show.legend = FALSE,
                 arrow = a, end_cap = circle(.07, 'inches')) +
  geom_node_point(color = "lightblue", size = 5) +
  geom_node_text(aes(label = name), vjust = 1, hjust = 1) +
  theme_void() +
  labs(title = "Common bigrams in the 資委會 corpus",
       subtitle = "n > 10",
       caption = "Based on Shenbao")

# save 

save.image('zwhtext.RData')
